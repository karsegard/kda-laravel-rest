<?php

namespace KDA\Rest;

use Closure;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use KDA\Rest\Client\Rest as ClientRest;

class Rest
{
    public function getClient($host=null){
        return app(ClientRest::class,['baseUrl'=>$host]);
    }
}
