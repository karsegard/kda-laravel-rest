<?php
namespace KDA\Rest\Collections;


class Response {
    public $body= null;
    public function __construct($response,$class=null){
        $this->status= $response->status();


        $this->data = json_decode($response->body(),true);
        //if(!$this->hasError()){
            if(!empty($class)){
                $this->body= new $class(json_decode($response->body(),true));
            }else{
                $this->body = $response->body();
            }
        //}
    }

    public function __get($key){
        return $this->data[$key];
    }

    public function hasError(){
        return $this->status <200 || $this->status > 308; 
    }

    public function errorMessage(){
        return $this->error;
    }

}
