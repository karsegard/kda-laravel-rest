<?php

namespace KDA\Rest\Collections;


class NestedObject
{
    public $body= null;

    public function __construct($data)
    {
        $this->body = $data;
        if (is_callable(['static', 'getAccessorKey'])) {

            $key = static::getAccessorKey();

            $this->data = collect($data)->diffKeys([$key => 1])->all();

            if (isset($data[$key])) {
                $this->$key = collect($data[$key])->map(function ($item) {
                    $class = static::getClass();
                    $o =  new $class($item);
                    if (method_exists($o, 'setParent')) {
                        $o->setParent($this);
                    }
                    return $o;
                });
            } else {
                $this->$key = collect([]);
            }
        } else if (is_callable(['static', 'getAccessorsKeys'])) {

            $keys = static::getAccessorsKeys();
            $diff_keys = array_fill_keys($keys, 1);
            $this->data = collect($data)->diffKeys($diff_keys)->all();
            foreach ($keys as $key) {

                $dest_key = $key;
                if (is_callable(['static', 'getNewKeyForKey'])) {
                    $new_key = static::getNewKeyForKey($key);
                    if ($new_key !== null) {
                        $dest_key = $new_key;
                    }
                }
                if (isset($data[$key])) {
                    $is_array = true;

                    if (is_callable(['static', 'getAccessorsIsArray'])) {
                        $is_array = static::getAccessorsIsArray($key);
                    }

                    if ($is_array) {
                        $this->$dest_key = collect($data[$key])->map(function ($item) use ($key) {
                            return $this->createObjectFromMultipleKeys($key, $item);
                        });
                    } else {
                        $item = $data[$key];
                        $this->$dest_key = $this->createObjectFromMultipleKeys($key, $item);
                    }
                } else {
                    $this->$dest_key = collect([]);
                }
            }
        } else {
            $this->data = $data;
        }
    }

    public function createObjectFromMultipleKeys($key, $item)
    {
        if (is_callable(['static', 'getClassByKey'])) {
            $class = static::getClassByKey($key);
            if (!empty($class)) {
                $o =  new $class($item);
                if (method_exists($o, 'setParentByKey')) {
                    $o->setParentByKey($this, $key);
                }
                return $o;
            } else {
                throw new \Exception('no class given for ' . $key);
            }
        } else {
            throw new \Exception('getClassByKey is not defined');
        }
    }


    public function __get($key)
    {
        return $this->data[$key];
    }
}
