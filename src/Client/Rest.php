<?php

namespace KDA\Rest\Client;

use Closure;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use KDA\Rest\Client\Concerns\EvaluatesClosure;
use KDA\Rest\Collections\Response;

class Rest
{
    use EvaluatesClosure;

    protected string | Closure | null $baseUrl;
    protected array | Closure $headers = [];

    protected $response;

    public function __construct(string | Closure | null $baseUrl=null){
        $this->baseUrl = $baseUrl;
    }

    public function getUrl($url){
        $base = $this->evaluate($this->baseUrl);
        return $base .$url;
    }

    public function getResponse($class=null){
        return new Response($this->response,$class);
    }


    public function request(string $method='get', string $url, array $data = [], array  $additional_headers = []) :static
    {
        $url = $this->getUrl($url);
       
        $req =  Http::withHeaders([
            ...$this->headers,
            ...$additional_headers
        ]);
        $this->response =  $req->$method($url, $data);
        return $this;
    }

    public function post($url,array  $data = [], array  $additional_headers = [])
    {
        return $this->request('post', $url, $data, $additional_headers);
    }
    public function get($url, array  $data = [], array  $additional_headers = [])
    {
        return $this->request('get', $url, $data, $additional_headers);
    }
    public function put($url, array  $data = [], array $additional_headers = [])
    {
        return $this->request('put', $url, $data, $additional_headers);
    }
}
