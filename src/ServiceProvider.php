<?php

namespace KDA\Rest;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Routing\Router;
use KDA\Laravel\PackageServiceProvider as BaseServiceProvider;
use KDA\Rest\Facades\Rest as Facade;
use KDA\Rest\Rest as Library;
class ServiceProvider extends BaseServiceProvider
{
    protected $packageName = 'laravel-rest';
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    protected $configDir = 'config';
    protected $configs = [
        'kda/rest.php'  => 'kda.rest'
    ];
    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
    }
     /**
     * called after the trait were registered
     */
    public function postRegister()
    {
    }
    //called after the trait were booted
    protected function bootSelf()
    {
    }
}
