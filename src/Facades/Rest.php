<?php

namespace KDA\Rest\Facades;

use Illuminate\Support\Facades\Facade;

class Rest extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
